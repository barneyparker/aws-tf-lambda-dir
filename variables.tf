variable "source_dir" {
  type = "string"
  description = "dir to create lambda zip"
}

variable "lambda_name" {
  type = "string"
  description = "Lambda Fucntion Name"
}

variable "handler" {
  type = "string"
  description = "Entrypoint Hanlder Function"
}

variable "runtime" {
  type = "string"
  description = "Lambda Runtime"
}

variable "environment" {
  type = "map"
  description = "Map of Environment Variables to pass to the lambda function"
  default = {
    module = "aws-tf-lambda-dir"
  }
}

variable "memory_size" {
  type = "string"
  description = "Memory allocation for the lambda"
  default = "128"
}

variable "timeout" {
  type = "string"
  description = "Timeout for the Lambda"
  default = "3"
}

variable "tags" {
  type = "map"
  description = "Tags to apply to the Lambda function"
  default = {}
}
